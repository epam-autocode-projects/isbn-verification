﻿using System;
using System.Text;

namespace StringVerification
{
    public static class IsbnVerifier
    {
        /// <summary>
        /// Verifies if the string representation of number is a valid ISBN-10 identification number of book.
        /// </summary>
        /// <param name="number">The string representation of book's number.</param>
        /// <returns>true if number is a valid ISBN-10 identification number of book, false otherwise.</returns>
        /// <exception cref="ArgumentException">Thrown if number is null or empty or whitespace.</exception>
        public static bool IsValid(string number)
        {
            int x, checksum = 0, j = 10, pc = 3;

            if (string.IsNullOrEmpty(number) || string.IsNullOrWhiteSpace(number))
            {
                throw new ArgumentException("bad one");
            }

            StringBuilder str = new StringBuilder();
            str.Append(number);

            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == '-')
                {
                    if (i < str.Length && str[i + 1] == '-')
                    {
                        return false;
                    }
                    else
                    {
                        str.Remove(i, 1);
                        pc--;
                        i--;
                    }
                }
                else if (!char.IsDigit(str[i]) && (i < str.Length - 1 || (i == str.Length - 1 && str[i] != 'X')))
                {
                    return false;
                }
            }

            if (str.Length != 10 || pc < 0)
            {
                return false;
            }

            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == 'X')
                {
                    x = 10;
                }
                else
                {
                    x = Convert.ToInt32(str[i].ToString(), null);
                }

                if (j >= 1)
                {
                    checksum += x * j;
                    j--;
                }
            }

            if (checksum % 11 == 0)
            {
                return true;
            }

            return false;
        }
    }
}